package com.projectswg.resources;

public enum ClientConnectionStatus {
	DISCONNECTED,
	LOGIN_CONNECTED,
	ZONE_CONNECTED
}
